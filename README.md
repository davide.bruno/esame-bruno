# GIT CHEAT SHEET COMANDI PRINCIPALI

Il seguente README ha lo scopo di elencare e spiegare alcuni comandi fondamentali di GIT


## INDICE 
1. [AUTHOR DETAILS](#AUTHOR-DETAILS)
2. [COMANDI BASE DELLA SHELL ](#COMANDI-BASE-DELLA-SHELL )
3. [GUIDA DEI COMANDI GIT ](#GUIDA-DEI-COMANDI-GIT )
4. [CHECKOUT](#CHECKOUT)
5. [MERGE](#MERGE)
6. [CONFLICT](#CONFLICT)
7. [GITLAB](#GITLAB)


## AUTHOR-DETAILS

1. Author name: Bruno Davide
2. Author email: davide.bruno@edu.itspiemonte.it

## COMANDI-BASE-DELLA-SHELL 

1. cd : per cambiare directory e muoversi all'interno di essa, con i due puntini .. si torna alla directory precedente.
2. mkdir: crea una directory
3. touch: crea un file
4. ls : lista e visualizza i file presenti  nella repository corrente 

## GUIDA-DEI-COMANDI-GIT 

### GIT INIT

Mi crea la directory.init
**Crea un magazzino del codice** (una directory chiamata .git), le informazioni più importanti di git
si trovano in questa repository 

### GIT ADD

Sintassi: git add  nome file
**Git add permette di selezionare un file, in questo modo viene "riconosciuto" da git**, in particolare
grazie a questo comando posso successivamente "commitare" le modifiche effettuate su questo file.

### GIT COMMIT

Descrizione di ciò che ho appena fatto,è una registrazione dell'azione appena 
fatta, **va effettuato ogni volta che si modifica il file** o se ne aggiunge uno
nuovo.Se il commento è piccolo posso effettuare git commit -m e scrivere
direttamente nella shell il commit senza aprire l'editor di testo. 

### GIT STATUS

Stato di Git,**verifica se ci sono commit da fare o file da aggiungere**
con il comando git add, o qualsiasi altra modifica effettuata rispetto
la versiome precedente(eliminazioni, file rinominati, etc ...). Se nel
repository git non ci sono problemi da come risultato il seguente output:
On branch master
nothing to commit, working tree clean.

### GIT LOG

**Visializzazione di tutti i Commit fatti**

OUTPUT:
commit 8d3281c4d8e95f9f54ab56e280420e27445a1f69 (HEAD -> master)

8d3281c4d8e95f9f54ab56e280420e27445a1f69: HASH codice univoco che viene assegnato al commit
 
HEAD-> MASTER: la HEAD punta  DOVE siamo adesso in quale punto della storia siamo

Author: Davide Bruno <davide.bruno@edu.itspiemonte.it> : autore
Date:   Tue Apr 11 09:55:37 2023 +0200 : data

    creato il file README.MD:  commento fatto

## CHECKOUT

**Permette di navigare la "storia" di git partendo da un determinato commit**
Sintassi: git checkout 8d3281c4d8e95f9f54ab56e280420e27445a1f69
          dove 8d3281c4d8e95f9f54ab56e280420e27445a1f69 è un hash univoco che viene assegnato ad ogni commit, ciò permette  git di individuarlo


### OUTPUT:

Note: switching to '8d3281c4d8e95f9f54ab56e280420e27445a1f69'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

git switch -c <new-branch-name>

Or undo this operation with:

git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 8d3281c CREATO README.MD

Facendo ORA GIT LOG si nota che  NON vengono tenuti IN CONSIDERAZIONE i commmit fatti prima del 
commit che ho passato a git checkout.Non c'e piu MASTER ma l'HASH del commit, la HEAD PUNTA 
Quest'ultima.


### MASTER

PER TORNARE AL PRESENTE; QUINDI QUANDO AVEVO TUTTI I COMMIT FARE:
GIT CHECKOUT MASTER  o MAIN.
Dove il **MASTER è il branch principale (dove si tengono le modifiche corrette, come un foglio di bella)**

### BRANCH

Un branch si può definire come un ramo della storia "parallela", vale a dire che quando si crea un branch
**si possono fare delle modifiche "sperimentali" senza alterare il MASTER O MAIN**, se il Master  equivale 
al foglio di bella **i BRANCH equvalgono ai fogli di brutta.** 

### GIT CHECKOUT -B NOMEBRANCH

Permette di creare nuovi branch per modificare e fare delle prove come fossero
fogli di brutta.


### GIT BRANCH

Visualizza tutti i branch  creati e anche il branch nel quale ti trovi


## MERGE


### GIT MERGE

**Permette di unire 2 o + storie diverse per fare un unico branch definitivo.**
Se si è nel master basta fare così:
git merge nomeBranch (nomeBranch + il master è sottointeso) se non 
si mettono altri parametri.Va utilizzato quando siamo soddisfatti di 
ciò che abbiamo fatto nel foglio di brutta, e dobbiamo riportarlo 
nel foglio di "bella"(master).

git log --graph --pretty=oneline: per la visualizzazione degli spostamenti dei commit con git log su windows.

## CONFLICT

Quando si scrivono in branch diversi nella stessa linea cose diverse.

### RISOLUZIONE CONFLITTI
 
Si possono risolvere a mano, se si apre il file si nota che vengono proposte
le due righe diverse, sta al programmatore scegliere il risultato che più si
addice al problema , si possono anche cancellare entrambe e scegliere una
terza soluzione, subito dopo la risoluzione del conflitto viene chiesto di fare 
un commit.



## GITLAB

### CREAZIONE DI UN NUOVO PROGETTO

Newproject---> blank project---> nome progetto--> eseguire questi comandi:

1. REMOTE: git remote add origin git@gitlab.com:davide.bruno/nomeprogetto.git


2. git push -u origin --all   Per inviare i file a GITLAB


### PULL E PUSH 


1. PUSH: Inviare delle modifiche al repository

   git push origin feat/branch: Serve per aggiungere il branch modificato o uno nuovo
   usando il remote a gitlab, il file in locale viene condiviso su un repository in
   remoto

2. PULL: Ricevere delle modifiche dal repository

   git pull origin master: Permette di prelevare i dati da un file
   origin--> da dove prende i dati; 
   master---> dove vengono importati i dati 
   il file in remoto viene copiato su un repository in
   locale.

### MERGE REQUEST

Richiesta di unire 2 BRANCH:
Funziona solo su GITLAB nella shell infatti usando git log non sarà
possibile vedere le modifiche. Per vedere queste si può
eseguire il pull.


### CLONE

Serve per creare una copia locale di un repository Git remoto per avere quel repository anche in locale 

Sintassi: git clone git@gitlab.com:davide.bruno/nome progetto.git 

